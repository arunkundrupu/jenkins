

# created matrix based authentication

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/01-jenkins-matrix.png)

# login in to user arun and created job

o/p: he is unable to delete the job as he is not authorised to delete as per matrix based authentication.

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/02-jenkins-matrix.png)



Project based matrix authorization:

# Restricted the authentication user arun for project

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/03-jenkins-project.png)

# Unable to access by user arun

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/04-jenkins-project.png)



#install role based authorization plugin



#Enable slack integration for jenkins jobs build notifications

#Install slack notification plugin

#Go to slack--&gt;settings--&gt;add an JENKINS as app integration

then add configurations :

post to channels - bootcamp(channel) and add it

#Now copy the base url, team domain and integration token in global Slack Notifier Settings in jenkins configure system.

#Add secret text in credentials option and run test connection.

#Go to job configuratin and under post-build actions select slack notification and choose the option as per requirment of build(start, fail, success, unstable, aborted, ect,.)

#Now run the build and check the status in boot camp

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/06-jenkins-slack.png)



# Gmail notification:

# Under jenkins configuration system go to email notification give the configuration details

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/07-jenkins-gmail.png)

# Run the test connection

# test connection failed due to disabled &quot;Allow less secure apps&quot;.

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/08-jenkins-gmail-error.png)

 so enabled it in gmail now test connection got success

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/09-jenkins-gmail-success.png)



# In jenkins job enabled the email notification and choose the option send mail for unstable build.

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day2-images/09-jenkins-email-success1.png)


