Create Job DSL for :

1. helloworld Job

2. hellotoperson Job (Take SALUTATION as choice parameter &amp; NAME as string parameter)

3. Gitclone and list content of cloned directory

4. buildperiodically Job (This job will run by every 5 min)

5. pollscm Job (This job will have a poll interval of 2 min)

6. upstream Job (This job have helloworld job as upstream)

7. downstream Job ( This job have hellotoperson job as downstream. Note: Pass variable(SALUTATION &amp; NAME) from this job to hellotoperson job.)

8. nestedview Job ( This job will create a nested view named as &#39;ninja-jobs&#39; with folders &#39;simple-jobs&#39; &amp; &#39;complex-jobs&#39;. simple-jobs will contain helloworld, hellotoperson, Gitclone, buildperiodically and pollscm. complex-jobs will contain upstream and downstream jobs.)

# Installed Job dsl plugin in jenkins

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/jenkins-dsl-plugin.png)

# created a job dsl-job to create hellowworld job

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/01%20jenkins-hello.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/02%20jenkins-hello-dsl.png)

#  created a job dsl-job to create hello person

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/02%20jenkins-person.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/02%20jenkins-person-dsl.png)

#  created a job dsl-job to create git-clone job

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/03%20jenkins-git-clone.png)

\* got the error due to git path not mentioned in global tools configuration

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/03%20jenkins-git-clone-error.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/03%20jenkins-git-clone-dsl.png)

#  created a job dsl-job to create build trigger periodically

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/04%20jenkins-triggger-periodical.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/04%20jenkins-trigger-periodic-dsl.png)

#  created a job dsl-job to create poll scm

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/05%20jenkins-poll.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/05%20jenkins-poll-dsl.png)

#  created a job dsl-job to create upstream

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/06%20jenkins-upstream.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/06%20jenkins-upstream-dsl.png)

#  created a job dsl-job to create downstream

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/07%20jenkins-downstream.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/07%20jenkins-downstream-dsl.png)

# Installed nested view plugin

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/08%20jenkins-dsl-nested-plugin.png)

#  created a job dsl-job to create nested view

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/08%20jenkins-nested.png)

![jenkins](https://github.com/arunkundrupu1990/jenkins/blob/master/day5-images/08%20jenkins-nested-dsl.png)


